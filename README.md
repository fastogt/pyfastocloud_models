About PyFastoCloud Models
===============

FastoCloud python mongodb models.

Dependencies
========
`setuptools` `mongoengine` `pyfastocloud_base`

Install
========
`python3 setup.py install`

