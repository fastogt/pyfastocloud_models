from datetime import datetime

import pyfastocloud_base.constants as constants
from mongoengine import EmbeddedDocument, fields, errors
from pyfastocloud_models.utils.utils import is_valid_url, date_to_utc_msec
from pyfastogt.maker import Maker


class Wpe(EmbeddedDocument, Maker):
    GL_FIELD = 'gl'
    gl = fields.BooleanField(default=False, required=True)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, gl = self.check_required_type(Wpe.GL_FIELD, bool, json)
        if res:
            self.gl = gl

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class Cef(EmbeddedDocument, Maker):
    GPU_FIELD = 'gpu'
    gpu = fields.BooleanField(default=False, required=True)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, gpu = self.check_required_type(Cef.GPU_FIELD, bool, json)
        if res:
            self.gpu = gpu

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class Url(EmbeddedDocument, Maker):
    ID_FIELD = 'id'
    URI_FIELD = 'uri'

    meta = {'allow_inheritance': True}

    _next_url_id = 0

    id = fields.IntField(default=lambda: Url.generate_id(), required=True)
    uri = fields.StringField(min_length=constants.MIN_URI_LENGTH, max_length=constants.MAX_URI_LENGTH, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        result.pop('_cls')
        return result.to_dict()

    @staticmethod
    def generate_id():
        current_value = Url._next_url_id
        Url._next_url_id += 1
        return current_value

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, id_field = self.check_required_type(Url.ID_FIELD, int, json)
        if res:
            self.id = id_field

        res, uri = self.check_required_type(Url.URI_FIELD, str, json)
        if res:
            if not constants.is_special_url(uri):
                if not is_valid_url(uri):
                    raise ValueError('Invalid url: {0}'.format(uri))
            self.uri = uri


class StreamLink(EmbeddedDocument, Maker):
    HTTP_PROXY_FIELD = 'http_proxy'
    HTTPS_PROXY_FIELD = 'https_proxy'
    PREFER_FIELD = 'prefer'

    http_proxy = fields.StringField(min_length=constants.MIN_URI_LENGTH, max_length=constants.MAX_URI_LENGTH,
                                    required=False, blank=True)
    https_proxy = fields.StringField(min_length=constants.MIN_URI_LENGTH, max_length=constants.MAX_URI_LENGTH,
                                     required=False, blank=True)
    prefer = fields.IntField(choices=constants.QualityPrefer.choices(), default=constants.QualityPrefer.QP_BOTH,
                             required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, pref = self.check_required_type(StreamLink.PREFER_FIELD, int, json)
        if res:
            self.prefer = pref

        res, http = self.check_optional_type(StreamLink.HTTP_PROXY_FIELD, str, json)
        if res:
            self.http_proxy = http
        else:
            delattr(self, StreamLink.HTTP_PROXY_FIELD)

        res, https = self.check_optional_type(StreamLink.HTTPS_PROXY_FIELD, str, json)
        if res:
            self.https_proxy = https
        else:
            delattr(self, StreamLink.HTTPS_PROXY_FIELD)


class SrtKey(EmbeddedDocument, Maker):
    PASSPHRASE_FIELD = 'passphrase'
    KEY_LEN_FIELD = 'pbkeylen'

    passphrase = fields.StringField(min_length=constants.MIN_URI_LENGTH, max_length=constants.MAX_URI_LENGTH,
                                    required=False, blank=True)
    pbkeylen = fields.IntField(required=False, blank=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, passw = self.check_optional_type(SrtKey.PASSPHRASE_FIELD, str, json)
        if res:
            self.passphrase = passw
        else:
            delattr(self, SrtKey.PASSPHRASE_FIELD)

        res, kl = self.check_optional_type(SrtKey.KEY_LEN_FIELD, int, json)
        if res:
            self.pbkeylen = kl
        else:
            delattr(self, SrtKey.KEY_LEN_FIELD)


class KVSProp(EmbeddedDocument, Maker):
    STREAM_NAME_FIELD = 'stream_name'
    ACCESS_KEY_FIELD = 'access_key'
    SECRET_KEY_FIELD = 'secret_key'
    AWS_REGION_FIELD = 'aws_region'
    STORAGE_SIZE_FIELD = 'storage_size'

    stream_name = fields.StringField(min_length=constants.MIN_STREAM_NAME_LENGTH,
                                     max_length=constants.MAX_STREAM_NAME_LENGTH,
                                     required=True)
    access_key = fields.StringField(required=True)
    secret_key = fields.StringField(required=True)
    aws_region = fields.StringField(required=True)
    storage_size = fields.IntField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, name = self.check_required_type(KVSProp.STREAM_NAME_FIELD, str, json)
        if res:
            self.stream_name = name

        res, key = self.check_required_type(KVSProp.ACCESS_KEY_FIELD, str, json)
        if res:
            self.access_key = key

        res, skey = self.check_required_type(KVSProp.SECRET_KEY_FIELD, str, json)
        if res:
            self.secret_key = skey

        res, aws = self.check_required_type(KVSProp.AWS_REGION_FIELD, str, json)
        if res:
            self.aws_region = aws

        res, stor = self.check_required_type(KVSProp.STORAGE_SIZE_FIELD, int, json)
        if res:
            self.storage_size = stor


class Programme(EmbeddedDocument, Maker):
    CHANNEL_FIELD = 'channel'
    START_FIELD = 'start'
    STOP_FIELD = 'stop'
    TITLE_FIELD = 'title'
    CATEGORY_FIELD = 'category'
    DESCRIPTION_FIELD = 'desc'

    channel = fields.StringField(min_length=constants.MIN_STREAM_TVG_ID_LENGTH,
                                 max_length=constants.MAX_STREAM_TVG_ID_LENGTH, required=True)
    start = fields.DateTimeField(default=datetime.utcfromtimestamp(0), required=True)
    stop = fields.DateTimeField(default=datetime.utcfromtimestamp(0), required=True)
    title = fields.StringField(min_length=constants.MIN_STREAM_NAME_LENGTH, max_length=constants.MAX_STREAM_NAME_LENGTH,
                               required=True)
    category = fields.StringField(min_length=constants.MIN_STREAM_TVG_ID_LENGTH,
                                  max_length=constants.MAX_STREAM_TVG_ID_LENGTH)
    desc = fields.StringField(min_length=constants.MIN_STREAM_DESCRIPTION_LENGTH,
                              max_length=constants.MAX_STREAM_DESCRIPTION_LENGTH)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def start_utc_msec(self):
        return date_to_utc_msec(self.start)

    def stop_utc_msec(self):
        return date_to_utc_msec(self.stop)

    def duration_msec(self):
        return self.stop_utc_msec() - self.start_utc_msec()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, channel = self.check_optional_type(Programme.CHANNEL_FIELD, str, json)
        if res:
            self.channel = channel

        res, start = self.check_required_type(Programme.START_FIELD, int, json)
        if res:
            self.start = datetime.utcfromtimestamp(start / 1000)

        res, stop = self.check_required_type(Programme.STOP_FIELD, int, json)
        if res:
            self.stop = datetime.utcfromtimestamp(stop / 1000)

        res, title = self.check_optional_type(Programme.TITLE_FIELD, str, json)
        if res:
            self.title = title

        res, category = self.check_optional_type(Programme.CATEGORY_FIELD, str, json)
        if res:  # optional field
            self.category = category
        else:
            delattr(self, Programme.CATEGORY_FIELD)

        res, desc = self.check_optional_type(Programme.DESCRIPTION_FIELD, str, json)
        if res:  # optional field
            self.desc = desc
        else:
            delattr(self, Programme.DESCRIPTION_FIELD)


class InputUrl(Url):
    USER_AGENT_FIELD = 'user_agent'
    STREAM_LINK_FIELD = 'stream_link'
    PROXY_FIELD = 'proxy'
    WPE_FIELD = 'wpe'
    PROGRAM_NUMBER_FIELD = 'program_number'
    MULTICAST_IFACE_FIELD = 'multicast_iface'
    SRT_MODE_FIELD = 'srt_mode'
    SRT_KEY_FIELD = 'srt_key'
    PROGRAMME_FIELD = 'programme'
    RTMPSRC_TYPE_FIELD = 'rtmpsrc_type'

    MIN_PROGRAM_NUMBER = 0
    MAX_PROGRAM_NUMBER = constants.MAX_INTEGER_NUMBER

    user_agent = fields.IntField(choices=constants.UserAgent.choices(), required=False)
    stream_link = fields.EmbeddedDocumentField(StreamLink, required=False)
    proxy = fields.StringField(required=False)
    wpe = fields.EmbeddedDocumentField(Wpe, required=False)
    program_number = fields.IntField(min_value=MIN_PROGRAM_NUMBER, max_value=MAX_PROGRAM_NUMBER, required=False)
    multicast_iface = fields.StringField(required=False)
    srt_mode = fields.IntField(choices=constants.SrtMode.choices(), required=False, blank=True)
    srt_key = fields.EmbeddedDocumentField(SrtKey, required=False)
    programme = fields.EmbeddedDocumentField(Programme, required=False)
    rtmpsrc_type = fields.IntField(choices=constants.RtmpSrcType.choices(), required=False, blank=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def make_stub(cls):
        return cls(id=Url.generate_id())

    def update_entry(self, json: dict):
        Url.update_entry(self, json)

        res, user_agent = self.check_optional_type(InputUrl.USER_AGENT_FIELD, int, json)
        if res:  # optional field
            self.user_agent = user_agent

        res, stream_link = self.check_optional_type(InputUrl.STREAM_LINK_FIELD, dict, json)
        if res:  # optional field
            self.stream_link = StreamLink.make_entry(stream_link)

        res, proxy = self.check_optional_type(InputUrl.PROXY_FIELD, str, json)
        if res:  # optional field
            self.proxy = proxy

        res, wpe = self.check_optional_type(InputUrl.WPE_FIELD, dict, json)
        if res:
            self.wpe = Wpe.make_entry(wpe)
        else:
            delattr(self, InputUrl.WPE_FIELD)

        res, program_number = self.check_optional_type(InputUrl.PROGRAM_NUMBER_FIELD, int, json)
        if res:  # optional field
            self.program_number = program_number

        res, multicast_iface = self.check_optional_type(InputUrl.MULTICAST_IFACE_FIELD, str, json)
        if res:  # optional field
            self.multicast_iface = multicast_iface

        res, srt_mode = self.check_optional_type(InputUrl.SRT_MODE_FIELD, int, json)
        if res:  # optional field
            self.srt_mode = srt_mode
        else:
            delattr(self, InputUrl.SRT_MODE_FIELD)

        res, srtkey = self.check_optional_type(InputUrl.SRT_KEY_FIELD, dict, json)
        if res:  # optional field
            self.srt_key = SrtKey.make_entry(srtkey)
        else:
            delattr(self, InputUrl.SRT_KEY_FIELD)

        res, programme = self.check_optional_type(InputUrl.PROGRAMME_FIELD, dict, json)
        if res:  # optional field
            self.programme = programme
        else:
            delattr(self, InputUrl.PROGRAMME_FIELD)

        res_rtmpsrc, rtmpsrc_type = self.check_optional_type(InputUrl.RTMPSRC_TYPE_FIELD, int, json)
        if res_rtmpsrc:
            self.rtmpsrc_type = rtmpsrc_type
        else:
            delattr(self, InputUrl.RTMPSRC_TYPE_FIELD)


class OutputUrl(Url):
    HTTP_ROOT_FIELD = 'http_root'
    CHUNK_DURATION_FIELD = 'chunk_duration'
    HLS_TYPE_FIELD = 'hls_type'
    HLSSINK_TYPE_FIELD = 'hlssink_type'
    TOKEN_FIELD = 'token'
    SRT_MODE_FIELD = 'srt_mode'
    SRT_KEY_FIELD = 'srt_key'
    PLAYLIST_ROOT_FIELD = 'playlist_root'
    RTMP_TYPE_FIELD = 'rtmp_type'
    RTMP_WEB_URL_FIELD = 'rtmp_web_url'
    RTMPSINK_TYPE_FIELD = 'rtmpsink_type'
    KVS_FIELD = 'kvs'

    # hls
    http_root = fields.StringField(min_length=constants.MIN_PATH_LENGTH, max_length=constants.MAX_PATH_LENGTH,
                                   required=False)
    token = fields.BooleanField(required=False, default=False)
    playlist_root = fields.StringField(min_length=constants.MIN_URI_LENGTH, max_length=constants.MAX_URI_LENGTH,
                                       required=False)
    chunk_duration = fields.IntField(required=False, blank=True)
    hlssink_type = fields.IntField(choices=constants.HlsSinkType.choices(), required=False, blank=True)
    hls_type = fields.IntField(choices=constants.HlsType.choices(), required=False, blank=True)
    # srt
    srt_mode = fields.IntField(choices=constants.SrtMode.choices(), required=False, blank=True)
    srt_key = fields.EmbeddedDocumentField(SrtKey, required=False)
    # rtmp
    rtmp_type = fields.IntField(choices=constants.RtmpType.choices(), required=False, blank=True)
    rtmp_web_url = fields.StringField(min_length=constants.MIN_URI_LENGTH, max_length=constants.MAX_URI_LENGTH,
                                      required=False)
    rtmpsink_type = fields.IntField(choices=constants.RtmpSinkType.choices(), required=False, blank=True)
    # kvs
    kvs = fields.EmbeddedDocumentField(KVSProp, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_hls(self):
        if self.hls_type is not None and self.http_root is not None and self.hlssink_type is not None:
            return True

        return False

    @classmethod
    def make_stub(cls):
        return cls(id=Url.generate_id())

    @classmethod
    def make_default_hls(cls):
        return cls(id=Url.generate_id(), hlssink_type=constants.HlsSinkType.HLSSINK, http_root='/',
                   hls_type=constants.HlsType.HLS_PULL)

    @classmethod
    def make_test(cls):
        return cls(id=Url.generate_id(), uri=constants.DEFAULT_TEST_URL)

    @classmethod
    def make_fake(cls):
        return cls(id=Url.generate_id(), uri=constants.DEFAULT_FAKE_URL)

    def update_entry(self, json: dict):
        Url.update_entry(self, json)

        res_root, http_root = self.check_optional_type(OutputUrl.HTTP_ROOT_FIELD, str, json)
        if res_root:
            self.http_root = http_root
        else:
            delattr(self, OutputUrl.HTTP_ROOT_FIELD)

        res_token, token = self.check_optional_type(OutputUrl.TOKEN_FIELD, bool, json)
        if res_token:
            self.token = token
        else:
            delattr(self, OutputUrl.TOKEN_FIELD)

        res_type, hls_type = self.check_optional_type(OutputUrl.HLS_TYPE_FIELD, int, json)
        if res_type:
            self.hls_type = hls_type
        else:
            delattr(self, OutputUrl.HLS_TYPE_FIELD)

        res_hlssink, hlssink_type = self.check_optional_type(OutputUrl.HLSSINK_TYPE_FIELD, int, json)
        if res_hlssink:
            self.hlssink_type = hlssink_type
        else:
            delattr(self, OutputUrl.HLSSINK_TYPE_FIELD)

        res_chunk, chunk_duration = self.check_optional_type(OutputUrl.CHUNK_DURATION_FIELD, int, json)
        if res_chunk:  # optional field
            self.chunk_duration = chunk_duration
        else:
            delattr(self, OutputUrl.CHUNK_DURATION_FIELD)

        res_playlist, playlist = self.check_optional_type(OutputUrl.PLAYLIST_ROOT_FIELD, str, json)
        if res_playlist:  # optional field
            self.playlist_root = playlist
        else:
            delattr(self, OutputUrl.PLAYLIST_ROOT_FIELD)

        res, srt_mode = self.check_optional_type(OutputUrl.SRT_MODE_FIELD, int, json)
        if res:  # optional field
            self.srt_mode = srt_mode
        else:
            delattr(self, OutputUrl.SRT_MODE_FIELD)

        res, srtkey = self.check_optional_type(OutputUrl.SRT_KEY_FIELD, dict, json)
        if res:  # optional field
            self.srt_key = SrtKey.make_entry(srtkey)
        else:
            delattr(self, OutputUrl.SRT_KEY_FIELD)

        res_rtmp_type, rtmp_type = self.check_optional_type(OutputUrl.RTMP_TYPE_FIELD, int, json)
        if res_rtmp_type:
            self.rtmp_type = rtmp_type
        else:
            delattr(self, OutputUrl.RTMP_TYPE_FIELD)

        res_rtmp_web_url, rtmp_web_url = self.check_optional_type(OutputUrl.RTMP_WEB_URL_FIELD, str, json)
        if res_rtmp_web_url:
            self.rtmp_web_url = rtmp_web_url
        else:
            delattr(self, OutputUrl.RTMP_WEB_URL_FIELD)

        res_rtmpsink, rtmpsink_type = self.check_optional_type(OutputUrl.RTMPSINK_TYPE_FIELD, int, json)
        if res_rtmpsink:
            self.rtmpsink_type = rtmpsink_type
        else:
            delattr(self, OutputUrl.RTMPSINK_TYPE_FIELD)

        res, kvs = self.check_optional_type(OutputUrl.KVS_FIELD, dict, json)
        if res:  # optional field
            self.kvs = KVSProp.make_entry(kvs)
        else:
            delattr(self, OutputUrl.KVS_FIELD)


class Point(EmbeddedDocument, Maker):
    X_FIELD = 'x'
    Y_FIELD = 'y'

    x = fields.IntField(required=True)
    y = fields.IntField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, x = self.check_required_type(Point.X_FIELD, int, json)
        if res:
            self.x = x

        res, y = self.check_required_type(Point.Y_FIELD, int, json)
        if res:
            self.y = y

    def __str__(self):
        return '{0},{1}'.format(self.x, self.y)


class Size(EmbeddedDocument, Maker):
    WIDTH_FIELD = 'width'
    HEIGHT_FIELD = 'height'

    INVALID_WIDTH = 0
    INVALID_HEIGHT = 0

    width = fields.IntField(required=True)
    height = fields.IntField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean(self):
        if self.width is None:
            raise errors.ValidationError('{0} should be not None'.format(Size.WIDTH_FIELD))

        if self.height is None:
            raise errors.ValidationError('{0} should be not None'.format(Size.HEIGHT_FIELD))

        if self.width <= Size.INVALID_WIDTH:
            raise errors.ValidationError('{0} should be bigger than {1}'.format(Size.WIDTH_FIELD, Size.INVALID_WIDTH))

        if self.height <= Size.INVALID_HEIGHT:
            raise errors.ValidationError('{0} should be bigger than {1}'.format(Size.HEIGHT_FIELD, Size.INVALID_HEIGHT))

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, width = self.check_required_type(Size.WIDTH_FIELD, int, json)
        if res:
            self.width = width

        res, height = self.check_required_type(Size.HEIGHT_FIELD, int, json)
        if res:
            self.height = height

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def __str__(self):
        return '{0}x{1}'.format(self.width, self.height)


class Logo(EmbeddedDocument, Maker):
    PATH_FIELD = 'path'
    POSITION_FIELD = 'position'
    ALPHA_FIELD = 'alpha'
    SIZE_FIELD = 'size'

    MIN_LOGO_ALPHA = 0.0
    MAX_LOGO_ALPHA = 1.0
    DEFAULT_LOGO_ALPHA = MAX_LOGO_ALPHA

    path = fields.StringField(required=True)
    position = fields.EmbeddedDocumentField(Point, required=True)
    alpha = fields.FloatField(min_value=MIN_LOGO_ALPHA, max_value=MAX_LOGO_ALPHA, required=True)
    size = fields.EmbeddedDocumentField(Size, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, path = self.check_required_type(Logo.PATH_FIELD, str, json)
        if res:
            self.path = path

        res, point = self.check_required_type(Logo.POSITION_FIELD, dict, json)
        if res:  # optional field
            self.position = Point.make_entry(point)

        res, alpha = self.check_required_type(Logo.ALPHA_FIELD, float, json)
        if res:  # optional field
            self.alpha = alpha

        res, size = self.check_required_type(Logo.SIZE_FIELD, dict, json)
        if res:  # optional field
            self.size = Size.make_entry(size)

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class RSVGLogo(EmbeddedDocument, Maker):
    PATH_FIELD = 'path'
    POSITION_FIELD = 'position'
    ALPHA_FIELD = 'alpha'
    SIZE_FIELD = 'size'

    path = fields.StringField(required=True)
    position = fields.EmbeddedDocumentField(Point, required=True)
    size = fields.EmbeddedDocumentField(Size, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, path = self.check_required_type(Logo.PATH_FIELD, str, json)
        if res:
            self.path = path

        res, point = self.check_required_type(Logo.POSITION_FIELD, dict, json)
        if res:  # optional field
            self.position = Point.make_entry(point)

        res, size = self.check_required_type(Logo.SIZE_FIELD, dict, json)
        if res:  # optional field
            self.size = Size.make_entry(size)

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class Rational(EmbeddedDocument, Maker):
    NUM_FIELD = 'num'
    DEN_FIELD = 'den'

    INVALID_RATIO_NUM = 0
    INVALID_RATIO_DEN = 0

    num = fields.IntField(required=True)
    den = fields.IntField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def clean(self):
        if self.num is None:
            raise errors.ValidationError('{0} should be not None'.format(Rational.NUM_FIELD))

        if self.den is None:
            raise errors.ValidationError('{0} should be not None'.format(Rational.DEN_FIELD))

        if self.num <= Rational.INVALID_RATIO_NUM:
            raise errors.ValidationError(
                '{0} should be bigger than {1}'.format(Rational.NUM_FIELD, Rational.INVALID_RATIO_NUM))

        if self.den <= Rational.INVALID_RATIO_NUM:
            raise errors.ValidationError(
                '{0} should be bigger than {1}'.format(Rational.DEN_FIELD, Rational.INVALID_RATIO_DEN))

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, num = self.check_required_type(Rational.NUM_FIELD, int, json)
        if res:
            self.num = num

        res, den = self.check_required_type(Rational.DEN_FIELD, int, json)
        if res:
            self.den = den

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def __str__(self):
        return '{0}:{1}'.format(self.num, self.den)


class HostAndPort(EmbeddedDocument, Maker):
    HOST_FIELD = 'host'
    PORT_FIELD = 'port'

    host = fields.StringField(required=True)
    port = fields.IntField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, host = self.check_required_type(HostAndPort.HOST_FIELD, str, json)
        if res:
            self.host = host

        res, port = self.check_required_type(HostAndPort.PORT_FIELD, int, json)
        if res:
            self.port = port

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def __str__(self):
        return '{0}:{1}'.format(self.host, self.port)


class MachineLearning(EmbeddedDocument, Maker):
    BACKEND_FILED = 'backend'
    MODEL_URL_FIELD = 'model_url'
    TRACKING_FIELD = 'tracking'
    DUMP_FIELD = 'dump'
    CLASS_ID_FIELD = 'class_id'
    OVERLAY_FIELD = 'overlay'

    backend = fields.IntField(choices=constants.MlBackends.choices(), required=True)
    model_url = fields.StringField(min_length=constants.MIN_URI_LENGTH, max_length=constants.MAX_URI_LENGTH,
                                   required=True)
    tracking = fields.BooleanField(required=True)
    class_id = fields.IntField(required=True)
    dump = fields.BooleanField(required=True)
    overlay = fields.BooleanField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, backend = self.check_required_type(MachineLearning.BACKEND_FILED, int, json)
        if res:
            self.backend = backend

        res, url = self.check_required_type(MachineLearning.MODEL_URL_FIELD, str, json)
        if res:
            if not is_valid_url(url):
                raise ValueError('Invalid url: {0}'.format(url))
            self.model_url = url

        res, tracking = self.check_required_type(MachineLearning.TRACKING_FIELD, bool, json)
        if res:
            self.tracking = tracking

        res, dump = self.check_required_type(MachineLearning.DUMP_FIELD, bool, json)
        if res:
            self.dump = dump

        res, clid = self.check_required_type(MachineLearning.CLASS_ID_FIELD, int, json)
        if res:
            self.class_id = clid

        res, overlay = self.check_required_type(MachineLearning.OVERLAY_FIELD, bool, json)
        if res:
            self.overlay = overlay

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class BackgroundEffect(EmbeddedDocument, Maker):
    TYPE_FILED = 'type'
    STRENGTH_FIELD = 'strength'
    IMAGE_FIELD = 'image'
    COLOR_FIELD = 'color'

    type = fields.IntField(choices=constants.BackgroundEffectType.choices(), required=True)
    strength = fields.FloatField(min=0.0, max=1.0, required=False)
    image = fields.StringField(required=False)
    color = fields.IntField(required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, type = self.check_required_type(BackgroundEffect.TYPE_FILED, int, json)
        if res:
            self.type = type

        res, strength = self.check_optional_type(BackgroundEffect.STRENGTH_FIELD, float, json)
        if res:
            self.strength = strength
        else:
            delattr(self, BackgroundEffect.STRENGTH_FIELD)

        res, image = self.check_optional_type(BackgroundEffect.IMAGE_FIELD, str, json)
        if res:
            self.image = image
        else:
            delattr(self, BackgroundEffect.IMAGE_FIELD)

        res, color = self.check_optional_type(BackgroundEffect.COLOR_FIELD, int, json)
        if res:
            self.color = color
        else:
            delattr(self, BackgroundEffect.COLOR_FIELD)

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class AudioStabilization(EmbeddedDocument, Maker):
    TYPE_FILED = 'type'
    GPU_MODEL_FILED = 'gpu_model'

    type = fields.IntField(choices=constants.AudioStabilizationType.choices(), required=True)
    gpu_model = fields.IntField(choices=constants.GpuModel.choices(), required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, audio_type = self.check_required_type(AudioStabilization.TYPE_FILED, int, json)
        if res:
            self.type = audio_type

        res, gpu_model = self.check_required_type(AudioStabilization.GPU_MODEL_FILED, int, json)
        if res:
            self.gpu_model = gpu_model

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class AlphaMethod(EmbeddedDocument, Maker):
    METHOD_FILED = 'method'
    ALPHA_FIELD = 'alpha'
    COLOR_FIELD = 'color'

    method = fields.IntField(choices=constants.AlphaMethodType.choices(), required=True)
    alpha = fields.FloatField(min=0.0, max=1.0, required=False)
    color = fields.IntField(required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, method = self.check_required_type(AlphaMethod.METHOD_FILED, int, json)
        if res:
            self.method = method

        res, alpha = self.check_optional_type(AlphaMethod.ALPHA_FIELD, float, json)
        if res:
            self.alpha = alpha
        else:
            delattr(self, AlphaMethod.ALPHA_FIELD)

        res, color = self.check_optional_type(AlphaMethod.COLOR_FIELD, int, json)
        if res:
            self.color = color
        else:
            delattr(self, AlphaMethod.COLOR_FIELD)

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class Font(EmbeddedDocument, Maker):
    FONT_FAMILY_FIELD = 'family'
    FONT_SIZE_FIELD = 'size'

    DEFAULT_FONT_SIZE = 18

    family = fields.StringField(required=True)
    size = fields.IntField(default=DEFAULT_FONT_SIZE, required=True)

    @classmethod
    def make_entry(cls, json: dict) -> 'Font':
        cl = cls()
        cl.update_entry(json)
        return cl

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, family = self.check_required_type(Font.FONT_FAMILY_FIELD, str, json)
        if res:
            self.family = family

        res, size = self.check_required_type(Font.FONT_SIZE_FIELD, int, json)
        if res:
            self.size = size

    def to_dict(self) -> dict:
        return {Font.FONT_FAMILY_FIELD: self.family, Font.FONT_SIZE_FIELD: self.size}


class TextOverlay(EmbeddedDocument, Maker):
    TEXT_FIELD = 'text'
    X_ABSOLUTE = 'x_absolute'
    Y_ABSOLUTE = 'y_absolute'
    FONT_FIELD = 'font'

    MAX_TEXT_LENGTH = 512
    MIN_TEXT_LENGTH = 1

    text = fields.StringField(max_length=MAX_TEXT_LENGTH, min_length=MIN_TEXT_LENGTH, required=True)
    x_absolute = fields.FloatField(min=0.0, max=1.0, required=True)
    y_absolute = fields.FloatField(min=0.0, max=1.0, required=True)
    font = fields.EmbeddedDocumentField(Font, required=False)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, text = self.check_required_type(TextOverlay.TEXT_FIELD, str, json)
        if res:
            self.text = text

        res, xabs = self.check_required_type(TextOverlay.X_ABSOLUTE, float, json)
        if res:
            self.x_absolute = xabs

        res, yabs = self.check_required_type(TextOverlay.Y_ABSOLUTE, float, json)
        if res:
            self.y_absolute = yabs

        res, font = self.check_optional_type(TextOverlay.FONT_FIELD, dict, json)
        if res:
            self.font = Font.make_entry(font)
        else:
            delattr(self, TextOverlay.FONT_FIELD)


class OverlayUrl(EmbeddedDocument, Maker):
    URL_FIELD = 'url'
    TYPE_FIELD = 'type'
    WPE_FIELD = 'wpe'
    CEF_FIELD = 'cef'

    MAX_TEXT_LENGTH = 512
    MIN_TEXT_LENGTH = 1

    url = fields.StringField(max_length=MAX_TEXT_LENGTH, min_length=MIN_TEXT_LENGTH, required=True)
    type = fields.IntField(choices=constants.OverlayUrlType.choices(), required=True)
    wpe = fields.EmbeddedDocumentField(Wpe, required=False)
    cef = fields.EmbeddedDocumentField(Cef, required=False)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, url = self.check_required_type(OverlayUrl.URL_FIELD, str, json)
        if res:
            self.url = url

        res, type = self.check_required_type(OverlayUrl.TYPE_FIELD, int, json)
        if res:
            self.type = type

        res, wpe = self.check_optional_type(OverlayUrl.WPE_FIELD, dict, json)
        if res:
            self.wpe = Wpe.make_entry(wpe)
        else:
            delattr(self, OverlayUrl.WPE_FIELD)

        res, cef = self.check_optional_type(OverlayUrl.CEF_FIELD, dict, json)
        if res:
            self.cef = Cef.make_entry(cef)
        else:
            delattr(self, OverlayUrl.CEF_FIELD)


class StreamOverlay(EmbeddedDocument, Maker):
    URL_FIELD = 'url'
    BACKGROUND_FIELD = 'background'
    METHOD_FIELD = 'method'
    SIZE_FIELD = 'size'

    url = fields.EmbeddedDocumentField(OverlayUrl, required=True)
    background = fields.IntField(choices=constants.BackgroundColor.choices(), required=False)
    method = fields.EmbeddedDocumentField(AlphaMethod, required=False)
    size = fields.EmbeddedDocumentField(Size, required=False)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, url = self.check_required_type(StreamOverlay.URL_FIELD, dict, json)
        if res:
            self.url = OverlayUrl.make_entry(url)

        res, back = self.check_optional_type(StreamOverlay.BACKGROUND_FIELD, int, json)
        if res:
            self.background = back
        else:
            delattr(self, StreamOverlay.BACKGROUND_FIELD)

        res, method = self.check_optional_type(StreamOverlay.METHOD_FIELD, dict, json)
        if res:
            self.method = AlphaMethod.make_entry(method)
        else:
            delattr(self, StreamOverlay.METHOD_FIELD)

        res, size = self.check_optional_type(StreamOverlay.SIZE_FIELD, dict, json)
        if res:  # optional field
            self.size = Size.make_entry(size)
        else:
            delattr(self, StreamOverlay.SIZE_FIELD)


class MetaUrl(EmbeddedDocument, Maker):
    NAME_FIELD = 'name'
    URL_FIELD = 'url'

    name = fields.StringField(min_length=constants.MIN_STREAM_NAME_LENGTH, max_length=constants.MAX_STREAM_NAME_LENGTH,
                              required=True)
    url = fields.StringField(max_length=constants.MAX_URI_LENGTH, min_length=constants.MIN_URI_LENGTH, required=True)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, name = self.check_required_type(MetaUrl.NAME_FIELD, str, json)
        if res:
            self.name = name

        res, url = self.check_required_type(MetaUrl.URL_FIELD, str, json)
        if res:
            if not is_valid_url(url):
                raise ValueError('Invalid url: {0}'.format(url))
            self.url = url


class Phone(EmbeddedDocument, Maker):
    DIAL_CODE_FIELD = 'dial_code'
    PHONE_NUMBER_FIELD = 'phone_number'
    ISO_CODE_FILED = 'iso_code'

    dial_code = fields.StringField(required=True)
    phone_number = fields.StringField(required=True)
    iso_code = fields.StringField(default=constants.DEFAULT_LOCALE, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, dial_code = self.check_required_type(Phone.DIAL_CODE_FIELD, str, json)
        if res:
            self.dial_code = dial_code

        res, phone_number = self.check_required_type(Phone.PHONE_NUMBER_FIELD, str, json)
        if res:
            self.phone_number = phone_number

        res, iso_code = self.check_required_type(Phone.ISO_CODE_FILED, str, json)
        if res:
            if not constants.is_valid_country_code(iso_code):
                raise ValueError('Invalid {0}'.format(Phone.ISO_CODE_FILED))
            self.iso_code = iso_code

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()


class StreamTTL(EmbeddedDocument, Maker):
    TTL_FIELD = 'ttl'
    PHOENIX_FIELD = 'phoenix'

    ttl = fields.IntField(min_value=constants.MIN_AUTO_EXIT_TIME, max_value=constants.MAX_AUTO_EXIT_TIME,
                          required=True)
    phoenix = fields.BooleanField(default=False, required=True)

    def is_valid(self) -> bool:
        try:
            self.validate()
        except errors.ValidationError:
            return False
        return True

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)
        res, ttl = self.check_required_type(StreamTTL.TTL_FIELD, int, json)
        if res:
            self.ttl = ttl

        res, phoenix = self.check_required_type(StreamTTL.PHOENIX_FIELD, bool, json)
        if res:
            self.phoenix = phoenix

    def to_front_dict(self) -> dict:
        result = self.to_mongo()
        return result.to_dict()
