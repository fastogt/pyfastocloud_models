from datetime import datetime
from enum import IntEnum

from bson import ObjectId
from mongoengine import Document, fields, EmbeddedDocument, errors, PULL
from pyfastocloud_models.common_entries import Maker
from pyfastocloud_models.utils.utils import date_to_utc_msec


class Subscription(Document, Maker):
    ID_FIELD = 'id'
    SID_FIELD = 'sid'
    STATUS_FIELD = 'status'
    CREATED_DATE_FIELD = 'created_date'

    meta = {'collection': 'subscription', 'allow_inheritance': False}

    @staticmethod
    def get_by_id(sid: ObjectId):
        return Subscription.objects.get(id=sid)

    class Status(IntEnum):
        NEW = 0
        OPEN = 1
        PAYED = 2

    sid = fields.StringField(required=True)
    status = fields.IntField(default=Status.NEW, required=True)
    created_date = fields.DateTimeField(default=datetime.now, required=True)  #
    server = fields.ReferenceField('ServiceSettings', blank=True)

    def get_id(self) -> str:
        return str(self.pk)

    def __init__(self, *args, **kwargs):
        super(Subscription, self).__init__(*args, **kwargs)

    @property
    def id(self):
        return self.pk

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, sid = self.check_required_type(Subscription.SID_FIELD, str, json)
        if res:
            self.sid = sid

        res, status = self.check_required_type(Subscription.STATUS_FIELD, int, json)
        if res:
            self.status = status

        res, created_date_msec = self.check_optional_type(Subscription.CREATED_DATE_FIELD, int, json)
        if res:  # optional field
            self.created_date = datetime.utcfromtimestamp(created_date_msec / 1000)

        try:
            self.validate()
        except errors.ValidationError as err:
            raise ValueError(err.message)

    def created_date_utc_msec(self):
        return date_to_utc_msec(self.created_date)

    def to_front_dict(self) -> dict:
        return {Subscription.ID_FIELD: self.get_id(), Subscription.SID_FIELD: self.sid,
                Subscription.STATUS_FIELD: self.status, Subscription.CREATED_DATE_FIELD: self.created_date_utc_msec()}
