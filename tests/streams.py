#!/usr/bin/env python3
import datetime
import unittest

from pyfastocloud_models.stream.entry import ProxyStream, RelayStream, EncodeStream, OutputUrl, InputUrl, CatchupStream, \
    TimeshiftRecorderStream


class StreamsTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def test_proxy(self):
        now = datetime.datetime.utcnow()
        stable = int(now.microsecond / 1000) * 1000
        now = now.replace(microsecond=stable)
        msec = int(now.timestamp() * 1000)
        output_url = OutputUrl(id=OutputUrl.generate_id(), uri='unknown://test')  # required
        name = 'Test'  # required
        proxy_data = {ProxyStream.NAME_FIELD: name, ProxyStream.GROUPS_FIELD: ['Movies', 'USA'],
                      ProxyStream.PRICE_FIELD: 1.1,
                      ProxyStream.CREATED_DATE_FIELD: msec, ProxyStream.OUTPUT_FIELD: [output_url.to_front_dict()]}
        proxy = ProxyStream.make_entry(proxy_data)
        self.assertEqual(proxy.name, proxy_data[ProxyStream.NAME_FIELD])
        self.assertEqual(proxy.groups, proxy_data[ProxyStream.GROUPS_FIELD])
        self.assertEqual(proxy.created_date_utc_msec(), msec)
        self.assertEqual(proxy.output, [output_url])
        self.assertTrue(proxy.is_valid())

    def test_relay(self):
        input_url = InputUrl(id=InputUrl.generate_id(), uri='unknown://test')  # required
        output_url = OutputUrl(id=OutputUrl.generate_id(), uri='unknown://test')  # required
        name = 'Relay'  # required
        relay_data = {RelayStream.NAME_FIELD: name, RelayStream.INPUT_FIELD: [input_url.to_front_dict()],
                      RelayStream.OUTPUT_FIELD: [output_url.to_front_dict()]}
        relay = RelayStream.make_entry(relay_data)
        self.assertEqual(relay.name, relay_data[ProxyStream.NAME_FIELD])
        self.assertEqual(relay.input, [input_url])
        self.assertEqual(relay.output, [output_url])
        self.assertTrue(relay.is_valid())

        prod = {"id": "5f2bac3de154540b4476c5d2", "name": "Stream",
                "tvg_logo": "https://fastocloud.com/images/unknown_channel.png", "groups": [], "type": 2, "price": 0,
                "view_count": 0, "output": [
                {"id": 0, "uri": "http://fastocloud.com:8000/2/5f2bac3de154540b4476c5d2/0/master.m3u8",
                 "http_root": "~/streamer/hls/2/5f2bac3de154540b4476c5d2/0", "hls_type": 0, "chunk_duration": 5}],
                "visible": True, "iarc": 18, "tvg_id": "123", "meta": [],
                "input": [{"id": 1, "uri": "http://localhost"}],
                "log_level": 6, "feedback_directory": "~/streamer/feedback/2/5f2bac3de154540b4476c5d2",
                "have_video": True, "have_audio": True, "phoenix": False, "loop": False, "restart_attempts": 10,
                "status": 0, "cpu": 0, "timestamp": 0, "idle_time": 0, "rss": 0,
                "loop_start_time": 0, "restarts": 0, "start_time": 0, "input_streams": [], "output_streams": [],
                "quality": 100, "video_parser": "h264parse", "audio_parser": "aacparse"}
        prod = RelayStream.make_entry(prod)
        self.assertEqual(prod.output[0].chunk_duration, 5)
        self.assertTrue(prod.is_valid())

    def test_catchup(self):
        input_url = InputUrl(id=InputUrl.generate_id(), uri='unknown://test')  # required
        output_url = OutputUrl(id=OutputUrl.generate_id(), uri='unknown://test')  # required
        name = 'Catchup'  # required
        relay_data = {RelayStream.NAME_FIELD: name, RelayStream.INPUT_FIELD: [input_url.to_front_dict()],
                      RelayStream.OUTPUT_FIELD: [output_url.to_front_dict()],
                      CatchupStream.START_RECORD_FIELD: 1614171498197,
                      CatchupStream.STOP_RECORD_FIELD: 1614172591293}
        catchup = CatchupStream.make_entry(relay_data)
        seconds = int(catchup.duration_msec() / 1000)
        self.assertEqual(seconds, 1093)
        self.assertEqual(catchup.name, relay_data[ProxyStream.NAME_FIELD])
        self.assertEqual(catchup.input, [input_url])
        self.assertEqual(catchup.output, [output_url])
        self.assertTrue(catchup.is_valid())

        prod = {"id": "5f2bac3de154540b4476c5d2", "name": "Stream",
                "tvg_logo": "https://fastocloud.com/images/unknown_channel.png", "groups": [], "type": 2, "price": 0,
                "view_count": 0, "output": [
                {"id": 0, "uri": "http://fastocloud.com:8000/2/5f2bac3de154540b4476c5d2/0/master.m3u8",
                 "http_root": "~/streamer/hls/2/5f2bac3de154540b4476c5d2/0", "hls_type": 0, "chunk_duration": 5}],
                "visible": True, "iarc": 18, "tvg_id": "123", "meta": [],
                "input": [{"id": 1, "uri": "http://localhost"}],
                "log_level": 6, "feedback_directory": "~/streamer/feedback/2/5f2bac3de154540b4476c5d2",
                "have_video": True, "have_audio": True, "phoenix": False, "loop": False, "restart_attempts": 10,
                "status": 0, "cpu": 0, "timestamp": 0, "idle_time": 0, "rss": 0,
                "loop_start_time": 0, "restarts": 0, "start_time": 0, "input_streams": [], "output_streams": [],
                "quality": 100, "video_parser": "h264parse", "audio_parser": "aacparse", "start": 123, "stop": 444}
        prod = CatchupStream.make_entry(prod)
        self.assertEqual(prod.output[0].chunk_duration, 5)
        self.assertTrue(prod.is_valid())

    def test_encode(self):
        input_url = InputUrl(id=InputUrl.generate_id(), uri='unknown://test')  # required
        output_url = OutputUrl(id=OutputUrl.generate_id(), uri='unknown://test')  # required
        name = 'Encode'  # required
        encode_data = {EncodeStream.NAME_FIELD: name, EncodeStream.INPUT_FIELD: [input_url.to_front_dict()],
                       EncodeStream.OUTPUT_FIELD: [output_url.to_front_dict()]}
        encode = EncodeStream.make_entry(encode_data)
        self.assertEqual(encode.name, encode_data[EncodeStream.NAME_FIELD])
        self.assertEqual(encode.input, [input_url])
        self.assertEqual(encode.output, [output_url])
        self.assertTrue(encode.is_valid())

    def test_raw(self):
        data = {"id": None, "name": "Stream", "tvg_logo": "https://fastocloud.com/images/unknown_channel.png",
                "groups": [], "type": 3, "price": 0, "view_count": 0, "output": [
                {"id": 0, "uri": "http://fastocloud.com:8000/master.m3u8", "http_root": "~/streamer/hls",
                 "hls_type": 0}], "visible": True, "iarc": 18, "tvg_id": None, "meta": [],
                "input": [{"id": 1, "uri": "http://localhost:8000/master.m3u8"}], "log_level": 6,
                "feedback_directory": None, "have_video": True, "have_audio": True, "phoenix": False, "loop": False,
                "restart_attempts": 10, "status": 0, "cpu": 0, "timestamp": 0, "idle_time": 0,
                "rss": 0, "loop_start_time": 0, "restarts": 0, "start_time": 0, "input_streams": [],
                "output_streams": [], "quality": 100, "relay_video": False, "relay_audio": False, "deinterlace": False,
                "volume": 1, "video_codec": "x264enc", "audio_codec": "faac"}

        encode = EncodeStream.make_entry(data)
        self.assertTrue(encode.is_valid())

    def test_recorder(self):
        data = {"id": "123", "name": "Stream", "tvg_logo": "https://fastocloud.com/images/unknown_channel.png",
                "groups": [], "type": 5, "price": 0, "view_count": 0, "output": [],
                "visible": False, "iarc": 18, "tvg_id": "", "meta": [], "description": "", "archive": False,
                "input": [{"id": 0, "uri": "https://stream.ecable.tv/afrobeats/tracks-v1a1/mono.m3u8"}], "log_level": 6,
                "feedback_directory": "~", "have_video": True, "have_audio": True, "relay_audio_type": 1,
                "relay_video_type": 1, "phoenix": False, "audio_tracks_count": 1, "loop": False, "restart_attempts": 10,
                "auto_start": False, "video_parser": "h264parse", "audio_parser": "aacparse"}

        time = TimeshiftRecorderStream.make_entry(data)
        self.assertTrue(time.is_valid())


if __name__ == '__main__':
    unittest.main()
