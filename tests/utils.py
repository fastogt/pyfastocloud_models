#!/usr/bin/env python3
import unittest

from pyfastocloud_models.utils.utils import is_valid_http_url


class UtilsTest(unittest.TestCase):
    def test_is_valid_http(self):
        self.assertTrue(is_valid_http_url('http://neczbm.to:80/images/2e48004d42d81180df8625ff3e9d5bb8.png'))


if __name__ == '__main__':
    unittest.main()
